//
//  CategoryCollectionViewCell.swift
//  Bazl
//
//  Created by Jorge Deshon on 5/8/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    
        var category: Category? {
            didSet {
                if let category = category {
                    self.categoryName.text = category.categoryName.toString()
//                    self.categoryImage.image = category.categoryName.itemImage()
                }
            }
        }
    
    // Use this method in case you want to make the cells dynamic in size.
    override func applyLayoutAttributes(layoutAttributes: UICollectionViewLayoutAttributes) {
        super.applyLayoutAttributes(layoutAttributes)
        //        if let attributes = layoutAttributes as? BazlLayoutAttributes {
        //            imageViewHeightLayoutConstraint.constant = attributes.photoHeight
        //        }
    }
    
}
