//
//  ItemDetailContainer.swift
//  Bazl
//
//  Created by Hector Mejia on 5/11/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import UIKit
import QuartzCore

class ItemDetailContainer: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    var item: Item!
    
    @IBOutlet weak var chatButon: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.r
        animateHideNavBar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        restoreNavBar()
    }

    func makeNavBarClear() {
    
        if let navBar = self.navigationController?.navigationBar {
            navBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
            navBar.shadowImage = UIImage()
            navBar.translucent = true
            self.navigationController?.view.backgroundColor = UIColor.clearColor()
            navBar.backgroundColor = UIColor.clearColor()
        } else {
            print("nav bar returned nil \(self.navigationController)")
        }
    
    }
    
    func restoreNavBar() {
        if let navBar = self.navigationController?.navigationBar {
            navBar.setBackgroundImage(nil, forBarMetrics: .Default)
        }
    }
    
    func animateShowNavBar() {
        if let navBar = self.navigationController?.navigationBar {
            navBar.setBackgroundImage(UIImage(color: bazlBlue), forBarMetrics: .Default)
            navBar.shadowImage = UIImage(color: bazlBlue)
            navBar.translucent = false
            self.navigationController?.view.backgroundColor = bazlBlue
            navBar.backgroundColor = bazlBlue
        } else {
            print("nav bar returned nil \(self.navigationController)")
        }
        
    }
    
    func animateHideNavBar() {
        let transition = CATransition()
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        transition.type = kCATransitionFade
        transition.duration = 0.5
        self.navigationController?.navigationBar.layer.addAnimation(transition, forKey: nil)
    }
    
 
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowItemTable" {
            let destination = segue.destinationViewController as! ItemDetailViewController
            destination.item = self.item
            destination.superContainerView = self
        }
    }

    @IBAction func chatButton(sender: UIButton) {
        print("Chatting")
    }

}
