//
//  CategorySegue.swift
//  Bazl
//
//  Created by Jorge Deshon on 5/12/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import UIKit

class CategorySegue: UIStoryboardSegue {
    
    override func perform() {
        let sourceVC = self.sourceViewController
        let destinationVC = self.destinationViewController
        
        sourceVC.view.addSubview(destinationVC.view)
        destinationVC.view.center.y = -destinationVC.view.center.y
        
        let currentWindow: UIWindow = UIApplication.sharedApplication().keyWindow!
        currentWindow.addSubview(destinationVC.view)
        
        
        print(destinationVC.view.center)
        
        UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseInOut, animations: {
            destinationVC.view.center.y = -destinationVC.view.center.y
        }) { (finished) in
            
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(0.001 * Double(NSEC_PER_SEC)))
            
            dispatch_after(time, dispatch_get_main_queue(), {
                
                sourceVC.presentViewController(destinationVC, animated: false, completion: nil)
            })
        }

    }
}
