//
//  CategoriesTableViewController.swift
//  Bazl
//
//  Created by Hector Mejia on 4/9/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import UIKit

protocol CategoriesTableViewControllerDelegate {
    func categoriesTableViewController(controller: CategoriesTableViewController, withCategory category: String)
}

// TODO: Make this dynamic
class CategoriesTableViewController: UITableViewController {
    
    // MARK: - Instance Variables
    
    // MARK: - Delegate
    var delegate: CategoriesTableViewControllerDelegate?
    var categoryList: [Category]!

    // MARK: - View Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadCategories()

    }
    
    func loadCategories() {
        categoryList = categories
        categoryList.removeFirst()
    }
 
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return categoryList.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CategoryCell", forIndexPath: indexPath)
        cell.textLabel?.text = categoryList[indexPath.row].categoryName.toString()
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        delegate?.categoriesTableViewController(self, withCategory: categoryList[indexPath.row].categoryName.toString())
    }
    
    func doTableRefresh()
    {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tableView.reloadData()
            return
        })
    }

}
