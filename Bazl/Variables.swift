//
//  Variables.swift
//  Bazl
//
//  Created by Hector Mejia on 3/8/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import Foundation
import UIKit
import AWSCore

let DEBUG = false

// Bazl's theme color
let bazlBlue = UIColor(red: 92/255.0, green: 204/255.0, blue: 250/255.0, alpha: 1.0)
let unselectedColor = UIColor(red: 194/255.0, green: 194/255.0, blue: 200/255.0, alpha: 1.0)

// Images
let tradesSelectedImage = UIImage(named: "trades selected")
let tradesNotSelectedImage = UIImage(named: "trades not selected")
let offersSelectedImage = UIImage(named: "offers selected")
let offersNotSelectedImage = UIImage(named: "offers not selected")

let itemImagePlaceHolders = [UIImage(named: "item placeholder1"),
                             UIImage(named: "item placeholder2"),
                             UIImage(named: "item placeholder3")]

// Bazl API URL
let apiURL = "https://lorostudios.com/api/bazl/search"
let categoriesURL = "https://lorostudios.com/api/bazl/categories"

// Bazl Categories
let categories = [Category(categoryName: Category.ItemCategory.All),
                  Category(categoryName: Category.ItemCategory.Electronics),
                  Category(categoryName: Category.ItemCategory.CarsAndMotors),
                  Category(categoryName: Category.ItemCategory.SportsAndLeisure),
                  Category(categoryName: Category.ItemCategory.BooksAndStationary),
                  Category(categoryName: Category.ItemCategory.HomeAndGarden),
                  Category(categoryName: Category.ItemCategory.Entertainment),
                  Category(categoryName: Category.ItemCategory.FashionAndAccessories),
                  Category(categoryName: Category.ItemCategory.Furniture),
                  Category(categoryName: Category.ItemCategory.Services)]

// AWS
let CognitoRegionType = AWSRegionType.USEast1  // e.g. AWSRegionType.USEast1
let DefaultServiceRegionType = AWSRegionType.USEast1 // e.g. AWSRegionType.USEast1
let CognitoIdentityPoolId = "us-east-1:26be075b-074c-4b82-9374-7f22b82276d9"
let S3BucketName = "bazl"
let s3ImageBaseUrl = "https://s3.amazonaws.com/bazl/"

// Create Item View Configuration
let itemTitleMaxCharacterCount = 50
let itemPriceMaxCharacterCount = 9


