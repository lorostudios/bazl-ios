//
//  ItemDetailViewController.swift
//  Bazl
//
//  Created by Hector Mejia on 4/17/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//


// TODO: List

/**
 
 - add segue for user and category
 - add item stats
 - add user's ratings
 
 **/

import UIKit
import MapKit
    import AWSS3



class ItemDetailViewController: UITableViewController {
    
    enum CellRow: Int {
        case ItemDetail = 0
        case ItemCategory = 1
        case ItemUser = 2
        case ItemLocation = 3
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imagesScrollView: UIScrollView!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userLocationLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var timeCreatedLabel: UILabel!
    @IBOutlet weak var viewsCountLabel: UILabel!
    @IBOutlet weak var favoritesCountLabel: UILabel!
    @IBOutlet weak var itemCategoryLabel: UILabel!
    @IBOutlet weak var itemDescription: UITextView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var itemDetailView: UIView!
    
    // Trades UIImage is tag number 1 inside acceptsTradesView
    // Trades UILabel is tag number 10 inside acceptsTradesView
    @IBOutlet weak var tradesAcceptedView: UIView!
    // Offers UIImage is tag number 1 inside acceptsOffersView
    // Offers UILabel is tag number 10 inside acceptsOffersView
    @IBOutlet weak var offersAcceptedView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imageHeighConstraint: NSLayoutConstraint!
    @IBOutlet weak var itemDescriptionHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var itemDetailCell: UITableViewCell!
        
    // MARK: - Instance variables
    
    private let kTableHeaderHeight: CGFloat = UIScreen.mainScreen().bounds.width
    var headerView: UIView!
    var superContainerView: ItemDetailContainer!
    
    var item: Item!
    
    var itemImageViews = [UIImageView]()
    
    
    // for preview 3D touch
    var image: UIImage?
    
    var itemTitleText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        
        setupUI()
    }

    
    func setupUI() {
        setupHeaderView()
        setupImageScrollView()
        configurePageControl()
        
        // Labels
        userImageView.layer.cornerRadius = userImageView.bounds.size.width/2
        itemTitle.text = item.itemTitle
        // userNameLabel.text = item.....
        // userLocationLabel.text = item....
        itemPriceLabel.text = "$\(item.itemPrice)"
        itemDescription.text = item.itemDescription
        itemCategoryLabel.text = item.itemCategory
        
        // Resizing necessary views
        resizeItemDescriptionTextView()
        
        
        
        
        
        // Setting up trades/offers
        if item.acceptsTrades == true {
            let tradesLabel = tradesAcceptedView.viewWithTag(10) as! UILabel
            let tradesImageView = tradesAcceptedView.viewWithTag(1) as! UIImageView
            tradesLabel.textColor = bazlBlue
            tradesImageView.image = tradesSelectedImage
        }
        
        if item.acceptsOffers == true {
            let tradesLabel = offersAcceptedView.viewWithTag(10) as! UILabel
            let tradesImageView = offersAcceptedView.viewWithTag(1) as! UIImageView
            tradesLabel.textColor = bazlBlue
            tradesImageView.image = offersSelectedImage
        }
    
    }
    
    func setupImageScrollView() {
        // Setting up images
        let listOfImages = item.getItemImages()
        
        let height = headerView.frame.height
        let width = tableView.bounds.width
        for index in 0..<listOfImages.count{
            
            let frame = CGRectMake((width * CGFloat(index)),
                                   0,
                                   width,
                                   height)
            
            self.imagesScrollView.pagingEnabled = true
            let imageView = UIImageView(frame: frame)
            imageView.contentMode = .ScaleAspectFill
            imageView.clipsToBounds = true
            
            imageView.contentMode = .ScaleAspectFill

            self.itemImageViews.append(imageView)
            self.imagesScrollView.addSubview(imageView)
            self.downloadImage(imageView, withKey: listOfImages[index])
            
        }
        
        self.imagesScrollView.contentSize = CGSizeMake(width * CGFloat(listOfImages.count),
                                                       self.imagesScrollView.frame.size.height)
        
    }
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        let numberOfImages = item.getItemImages().count
        if numberOfImages > 1 {
            self.pageControl.numberOfPages = item.getItemImages().count
            self.pageControl.currentPage = 0
            self.pageControl.addTarget(self, action: #selector(ItemDetailViewController.changePage(_:)), forControlEvents: UIControlEvents.ValueChanged)
        } else {
            self.pageControl.hidden = true
        }
        
    }
    
    func setupHeaderView() {
        // Setting up header and footer for stretchy header
        let frame = CGRectMake(0, 0, tableView.bounds.width, 80)
        tableView.tableFooterView = UIView(frame: frame)
        headerView = tableView.tableHeaderView
        tableView.tableHeaderView = nil
        tableView.addSubview(headerView)
        
//        let navigationBarSize = self.navigationController?.navigationBar.frame.size.height
//        tableView.contentInset = UIEdgeInsets(top: kTableHeaderHeight - navigationBarSize!, left: 0, bottom: 0, right: 0)
        tableView.contentInset = UIEdgeInsets(top: kTableHeaderHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -kTableHeaderHeight)
        updateHeaderView()
    }
    
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * imagesScrollView.frame.size.width
        imagesScrollView.setContentOffset(CGPointMake(x, 0), animated: true)
    }
    
    func updateHeaderView() {
        var headerRect = CGRect(x: 0,
                                y: -kTableHeaderHeight,
                                width: tableView.bounds.width,
                                height: kTableHeaderHeight)
        
        if tableView.contentOffset.y < -kTableHeaderHeight {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y
            for imageView in itemImageViews {
                imageView.frame.size.height = -tableView.contentOffset.y
            }
        }
        
        headerView.frame = headerRect

    }
    
    
    func resizeItemDescriptionTextView()  {
        let sizeThatFits = itemDescription.intrinsicContentSize()
        
        let sizeToBeUsed = sizeThatFits.height > itemDescription.frame.size.height ? sizeThatFits.height : itemDescription.frame.size.height
        
        itemDescriptionHeightConstraint.constant = sizeToBeUsed
        
    }
    
    func makeNavBarClear() {
        
        if let navBar = self.navigationController?.navigationBar {
            navBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
            navBar.shadowImage = UIImage()
            navBar.translucent = true
            self.navigationController?.view.backgroundColor = UIColor.clearColor()
            navBar.backgroundColor = UIColor.clearColor()
            print("clear nav")
        } else {
            print("nav bar returned nil \(self.navigationController)")
        }
        
    }
    
    func restoreNavBar() {
        if let navBar = self.navigationController?.navigationBar {
            navBar.setBackgroundImage(nil, forBarMetrics: .Default)
        }
    }
    
    // MARK: - Tableview delegates

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.selected = false
        
        switch indexPath.row {
        case CellRow.ItemDetail.rawValue:
            print("Item Description tapped")
        case CellRow.ItemCategory.rawValue:
            print("Item Category tapped")
        case CellRow.ItemUser.rawValue:
            print("Item User tapped")
        case CellRow.ItemLocation.rawValue:
            print("Item Location tapped")
        default:
            print("Unknown Cell tapped")
        }
        
    }
    
    // MARK: - Scrollview delegates
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if scrollView.isEqual(self.imagesScrollView) {
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl.currentPage = Int(pageNumber)
        }
        
        if scrollView.isEqual(self.tableView) {
            let contentOffset: CGPoint = CGPointMake(0.0, min(scrollView.contentOffset.y, -120.0))
            if contentOffset.y == -120 {
                UIView.animateWithDuration(0.2, animations: {
                    self.superContainerView.animateShowNavBar()
                print("show bar")
                })
            } else {
                UIView.animateWithDuration(0.2, animations: {
                    print("hide")
                    self.superContainerView.makeNavBarClear()
                })
            }
            
        }
        
        updateHeaderView()
    }
    
    // MARK: - AWS S3
    func downloadImage(imageView: UIImageView, withKey key: String){
        
        var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
        
        completionHandler = { (task, location, data, error) -> Void in
            print("download finished")
            dispatch_async(dispatch_get_main_queue(), {
                if ((error) != nil){
                    print("Failed with error")
                    print("Error: \(error!)")
                }
                else{
                    //Set your image
                    if let data = data, img = UIImage(data: data) {
                        imageView.image = img
                    }
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.defaultS3TransferUtility()
        transferUtility.downloadDataFromBucket(S3BucketName, key: key, expression: nil, completionHander: completionHandler).continueWithBlock { (task) -> AnyObject? in
            if let error = task.error {
                print("Error: \(error.localizedDescription)")
            }
            if let exception = task.exception {
                print("Exception: \(exception.description)")
            }
            if let _ = task.result {
                print("Download Starting!")
            }
            return nil;
        }
        
    }
    
    // MARK: - Outlet actions
    @IBAction func navMoreButton(sender: AnyObject) {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let shareAction = UIAlertAction(title: "Share", style: .Default) { (alert) in
            //do stuff here
            
            var sharingItems = [AnyObject]()
            let sharingMessage = "Check out this \(self.item.itemTitle) that I found in Bazl for $\(self.item.itemPrice).\n <url to item goes here>"
            
            if let image = self.itemImageViews.first?.image {
                sharingItems.append(image)
            }
            
            sharingItems.append(sharingMessage)
            
            let shareMenu = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
            self.presentViewController(shareMenu, animated: true, completion: nil)
            
            
        }
        
        let reportAction = UIAlertAction(title: "Report listing", style: .Destructive) { (alert) in
            // present report vc here
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(shareAction)
        optionMenu.addAction(reportAction)
        optionMenu.addAction(cancelAction)
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
    }
    
    func chatButtonPressed(sender: UIButton) {
        print("chat pressed")
    }
    
}














