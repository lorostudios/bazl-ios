//
//  CategoryViewController.swift
//  Bazl
//
//  Created by Jorge Deshon on 5/2/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import UIKit

protocol CategoryViewControllerDelegate {
    func categoryViewController(controller: CategoryViewController, didSelectCategoryFromCategoryList category: String, withAPIurl apiURL:String)
    func categoryViewControllerDidCancel(controller: CategoryViewController)
    
}

class CategoryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // MARK: - Outlets
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    // MARK: Instance Variables
    
    
    // MARK: - Delegate variables
    var delegate: CategoryViewControllerDelegate?
    
    // MARK: - View functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        //         Get the new view controller using segue.destinationViewController.
        //         Pass the selected object to the new view controller.
        if segue.identifier == "categoryShowDismiss"
        {
            print("segue")
            
        }
    }
    
    
    
    // MARK: - IBAction
    
    @IBAction func done() {
        self.delegate?.categoryViewControllerDidCancel(self)
    }
    
    @IBAction func comeHereOnLogout(segue:UIStoryboardSegue) {
        print("Yay, Logged Out!")
    }
    
    // MARK: - Collection View
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("categoryCell", forIndexPath: indexPath) as! CategoryCollectionViewCell
        cell.category = categories[indexPath.row]
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(categories.count)
        return categories.count
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let category = categories[indexPath.row]
        let categoryName = category.categoryName.toString()
        let apiURL = getAPIUrl(category)
        self.delegate?.categoryViewController(self, didSelectCategoryFromCategoryList: categoryName, withAPIurl: apiURL)

    }
    
    func getAPIUrl(category: Category) -> String{
        
        switch category.categoryName {
        case .All:
            return apiURL
        default:
            return "https://lorostudios.com/api/bazl/search?category=\(category.categoryName.toString())"
        }
        
    }
    
    
    /*
     UICOLLECTIONVIEW DOES NOT LOAD DATA FIRST TIME AROUND,
     THIS FUNCTION RELOADS THE DATA AFTER ALL THE CATEGORIES ARE
     PULLED FROM THE DATABASE
     */
    func doCollectionRefresh()
    {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.categoryCollectionView.reloadData()
            print("Refreshing table")
            return
        })
    }
    
    
}
