//
//  CreateListingViewController.swift
//  Bazl
//
//  Created by Hector Mejia on 3/10/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import UIKit
import MobileCoreServices

#if os(iOS)
    import AWSS3
#endif


protocol CreateListingViewControllerDelegate {
    func createListingViewController(controller: CreateListingViewController, didFinishCreatingPostForItem item: Item)
    func createListingViewControllerDidCancel(controller: CreateListingViewController)
}


class CreateListingViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    
    // TODO: ADD GESTURES TO ALL THE VIEWS, DONT FIND BY LOCATION
    
    // MARK: - Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContentView: UIView!
    @IBOutlet var itemImages: [UIImageView]!
    @IBOutlet weak var shareOnFacebook: UISwitch!
    
    // Title TextField is tag number 20
    @IBOutlet weak var itemTitleTextField: UITextField!
    // Price TextField is tag number 21
    @IBOutlet weak var itemPriceTextField: UITextField!
    // Description TextField is tag number 22
    @IBOutlet weak var itemDescriptionTextField: UITextField!
    
    @IBOutlet weak var itemDescriptionTextView: UITextView!
    @IBOutlet weak var itemInfoContainerView: UIView!
    
    
    // Condition UILabel has tag number 1, retreive that way
    @IBOutlet weak var conditionSelectorView: UIView!
    // Category UILabel has tag number 1, retreive that way
    @IBOutlet weak var categorySelectorView: UIView!
    
    // Trades UIImage is tag number 10 inside acceptsTradesView
    // Trades UILabel is tag number 1 inside acceptsTradesView
    @IBOutlet weak var acceptsTradesView: UIView!
    // Offers UIImage is tag number 10 inside acceptsOffersView
    // Offers UILabel is tag number 1 inside acceptsOffersView
    @IBOutlet weak var acceptsOffersView: UIView!
    
    
    // MARK: - Instance variables
    
    // Item config
    var images = [UIImage]()
    var uploadFileURLs = [String]()
    var itemCondition: Item.ItemCondition?
    var itemCategory: String?
    var tradesSelected = false
    var offersSelected = false
    
    // VC stuff
    var placeHolderImage = UIImage(named: "placeholder")
    var tappedImage: UIImageView?
    var activeView: UIView?
    var didEditItem = false
    let kPlaceHolderText = "Write a short description"
    var progressHUD: ProgressHUD?
    
    // MARK: - Delegate variables
    var delegate: CreateListingViewControllerDelegate?
    
    // MARK: - Views functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createTempUploadDir()
        registerForTouchEvents()
        registerForKeyboardNotifications()
        applyPlaceholderStyleToTextView(itemDescriptionTextView, placeHolderText: kPlaceHolderText)
    }
    
    // MARK: - View setup functions
    
    /**
        Registers tap events on scroll view
     */
    func registerForTouchEvents() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CreateListingViewController.handleTouches(_:)))
        tapGesture.cancelsTouchesInView = false
        scrollView.addGestureRecognizer(tapGesture)
    }
    
    /**
        Registers notifications for keyboard
     */
    func registerForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(CreateListingViewController.keyboardWasShown(_:)),
                                                         name: UIKeyboardDidShowNotification,
                                                         object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(CreateListingViewController.keyboardWillBeHidden(_:)),
                                                         name: UIKeyboardWillHideNotification,
                                                         object: nil)
    }
    
    
    
    // MARK: - Keyboard handling
    
    /**
        Called when the UIKeyboardDidShowNotification is sent
        
        - Parameter notification: notification sent when the keyboard is shown, sent by the OS
     */
    func keyboardWasShown(notification: NSNotification) {
        if let info = notification.userInfo {
            if let keyboardSize = info[UIKeyboardFrameBeginUserInfoKey]?.CGRectValue().size {
                let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
                self.scrollView.contentInset = contentInsets
                self.scrollView.scrollIndicatorInsets = contentInsets
                
                // If active text field is hidden by keyboard, scroll it so it's visible
                var viewRect = self.view.frame
                viewRect.size.height -= keyboardSize.height
                if let activeView = activeView {
                    print("activeView exists")
                    let rect = self.view.convertRect(activeView.frame, fromView: itemInfoContainerView)
                    if (!CGRectContainsPoint(viewRect, CGPointMake(rect.maxX, rect.maxY))) {
                        print("in if in keyboardWasShown")
                        self.scrollView.scrollRectToVisible(rect, animated: true)
                    }
                }
            }
            
        }
        
    }
    
    /**
        Called when the UIKeyboardWillHideNotification is sent
     
        - Parameter notification: notification sent when the keyboard will hide, sent by the OS
     */
    func keyboardWillBeHidden(notification: NSNotification) {
        let contentInsets = UIEdgeInsetsZero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    /**
        Hides keyboard.
     */
    func hideKeyboard() {
        if itemPriceTextField.isFirstResponder() {
            itemPriceTextField.resignFirstResponder()
        }
        if itemTitleTextField.isFirstResponder() {
            itemTitleTextField.resignFirstResponder()
        }
        if itemDescriptionTextView.isFirstResponder() {
            itemDescriptionTextView.resignFirstResponder()
        }
        
    }
    
    
    // MARK: - Gesture handlers
    
    /**
        Called when a tap is registered in the scroll view
     
        - Parameter touch: Gesture recognizer passed by the OS
     */
    func handleTouches(touch: UITapGestureRecognizer) {
        
        let location = touch.locationInView(self.view)
        for image in itemImages {
            let imageViewFrame = self.view.convertRect(image.frame, fromView: image.superview)
            if CGRectContainsPoint(imageViewFrame, location) {
                print("image tapped: index \(image.tag)")
                tappedImage = image
                
                imageViewTapped()
            }
        }
        
        let tradesViewFrame = self.view.convertRect(acceptsTradesView.frame, fromView: itemInfoContainerView)
        if CGRectContainsPoint(tradesViewFrame, location) {
            print("trades touched")
            if !tradesSelected {
                let tradesLabel = acceptsTradesView.viewWithTag(1) as! UILabel
                let tradesImageView = acceptsTradesView.viewWithTag(10) as! UIImageView
                tradesLabel.textColor = bazlBlue
                tradesImageView.image = tradesSelectedImage
                tradesSelected = true
                didEditItem = true
            }else {
                let tradesLabel = acceptsTradesView.viewWithTag(1) as! UILabel
                let tradesImageView = acceptsTradesView.viewWithTag(10) as! UIImageView
                tradesLabel.textColor = unselectedColor
                tradesImageView.image = tradesNotSelectedImage
                tradesSelected = false
            }
        }
        
        let offersViewFrame = self.view.convertRect(acceptsOffersView.frame, fromView: itemInfoContainerView)
        if CGRectContainsPoint(offersViewFrame, location) {
            print("offers touched")
            if !offersSelected {
                let tradesLabel = acceptsOffersView.viewWithTag(1) as! UILabel
                let tradesImageView = acceptsOffersView.viewWithTag(10) as! UIImageView
                tradesLabel.textColor = bazlBlue
                tradesImageView.image = offersSelectedImage
                offersSelected = true
                didEditItem = true
            }else {
                let tradesLabel = acceptsOffersView.viewWithTag(1) as! UILabel
                let tradesImageView = acceptsOffersView.viewWithTag(10) as! UIImageView
                tradesLabel.textColor = unselectedColor
                tradesImageView.image = offersNotSelectedImage
                offersSelected = false
            }
        }
        
        let conditionViewFrame = self.view.convertRect(conditionSelectorView.frame, fromView: itemInfoContainerView)
        if CGRectContainsPoint(conditionViewFrame, location) {
            print("condition view tapped")
            showConditions()
        }
        
        let categoryViewFrame = self.view.convertRect(categorySelectorView.frame, fromView: itemInfoContainerView)
        if CGRectContainsPoint(categoryViewFrame, location) {
            print("category view tapped")

            self.performSegueWithIdentifier("SelectCategory", sender: self)
        }
        
        
    }

    
    /**
        Displays a `UIAlerController` with all the conditons to be selected
     */
    func showConditions() {
        
        // Mint Great Used Fair Worn
        let optionMenu = UIAlertController(title: "Conditions", message: nil, preferredStyle: .ActionSheet)
        let conditionLabel = self.conditionSelectorView.viewWithTag(1) as! UILabel
        
        let mintAction = UIAlertAction(title: "Mint", style: .Default) { (alert: UIAlertAction!) in
            conditionLabel.text = "Mint"
            self.didEditItem = true
            self.itemCondition = .Mint
        }
        let greatAction = UIAlertAction(title: "Great", style: .Default) { (alert: UIAlertAction!) in
            conditionLabel.text = "Great"
            self.didEditItem = true
            self.itemCondition = .Great
        }
        let usedAction = UIAlertAction(title: "Used", style: .Default) { (alert: UIAlertAction!) in
            conditionLabel.text = "Used"
            self.didEditItem = true
            self.itemCondition = .Used
        }
        let fairAction = UIAlertAction(title: "Fair", style: .Default) { (alert: UIAlertAction!) in
            conditionLabel.text = "Fair"
            self.didEditItem = true
            self.itemCondition = .Fair
        }
        let wornAction = UIAlertAction(title: "Worn", style: .Default) { (alert: UIAlertAction!) in
            conditionLabel.text = "Worn"
            self.didEditItem = true
            self.itemCondition = .Worn
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(mintAction)
        optionMenu.addAction(greatAction)
        optionMenu.addAction(usedAction)
        optionMenu.addAction(fairAction)
        optionMenu.addAction(wornAction)
        optionMenu.addAction(cancelAction)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
        
    }
    
    /**
        Takes the selected image and assigns the picked image from the user.
     */
    func imageViewTapped() {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        if let image = tappedImage?.image {
            if !image.isEqual(placeHolderImage) {
                let deleteImage = UIAlertAction(title: "Delete", style: .Default, handler: { (alert: UIAlertAction!) in
                    // remove the image desired
                    self.images.removeAtIndex(self.tappedImage!.tag)
                    
                    // reset all the image views
                    for imageView in self.itemImages {
                        if !imageView.image!.isEqual(self.placeHolderImage) {
                            imageView.image = nil
                            imageView.image = self.placeHolderImage
                        }
                    }
                    
                    // reorder the images
                    for i in 0..<self.images.count {
                        self.itemImages[i].image = self.images[i]
                    }
            
                })
                optionMenu.addAction(deleteImage)
            }
        }
        
        let takePictureAction = UIAlertAction(title: "Take a picture", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Taking picture")
            
            if UIImagePickerController.isSourceTypeAvailable(.Camera) {
                if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .Camera
                    imagePicker.cameraCaptureMode = .Photo
                    self.presentViewController(imagePicker, animated: true, completion: nil)
                }
            }
            
        })
        
        let chooseImageAction = UIAlertAction(title: "Choose existing image", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Choosing image")
            
            if UIImagePickerController.isSourceTypeAvailable(.SavedPhotosAlbum) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .PhotoLibrary
                imagePicker.allowsEditing = false
                self.presentViewController(imagePicker, animated: true,
                    completion: nil)
            }
            
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(takePictureAction)
        optionMenu.addAction(chooseImageAction)
        optionMenu.addAction(cancelAction)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "SelectCategory" {
            let destination = segue.destinationViewController as! CategoriesTableViewController
            destination.delegate = self
            destination.navigationItem.title = "Categories"
        }
        
    }
    
    
    
    // MARK: - Outlet functions
    @IBAction func closeButtonPressed(sender: UIBarButtonItem) {
        hideKeyboard()
        
        if didEditItem {
            let optionsMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
            
            let cancelEditing = UIAlertAction(title: "Cancel and go back", style: .Default) { (alert: UIAlertAction!) in
                self.delegate?.createListingViewControllerDidCancel(self)
            }
            let continueEditing = UIAlertAction(title: "Continue Editing", style: .Cancel) { (alert: UIAlertAction!) in
                print("continue editing")
            }
            
            optionsMenu.addAction(cancelEditing)
            optionsMenu.addAction(continueEditing)
            presentViewController(optionsMenu, animated: true, completion: nil)
        } else {
            self.delegate?.createListingViewControllerDidCancel(self)
        }
        
        
    }
    
    @IBAction func bazlButtonPressed(sender: UIButton) {
        
        // Checking if there is any field is empty
        
        if images.count == 0 {
            displayAlertMessage("Missing Image", withAlertDescription: "Please attach at least one image", sender: self)
            return
        } else if itemTitleTextField.text == "" {
            displayAlertMessage("Missing Title", withAlertDescription: "Please include a title", sender: self)
            return
        } else if itemPriceTextField.text == "" {
            displayAlertMessage("Missing Price", withAlertDescription: "Please include a price", sender: self)
            return
        } else if itemCondition == nil {
            displayAlertMessage("Missing Condition", withAlertDescription: "Please select a condition", sender: self)
            return
        } else if itemCategory == nil {
            displayAlertMessage("Missing Category", withAlertDescription: "Please select a category", sender: self)
            return
        }
        
        // No issues, commense submission
        
        displayActivityIndicatorView()
        
        for image in images {
            let fileName = NSProcessInfo.processInfo().globallyUniqueString.stringByAppendingString(".jpg")
            let fileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("upload").URLByAppendingPathComponent(fileName)
            let filePath = fileURL.path!
//            let imageData = UIImageJPEGRepresentation(image, 0.3)
            let imageData = image.lowestQualityJPEGNSData
            let result = imageData.writeToFile(filePath, atomically: true)
            
            print("\nresult: [\(result)] \n\nfile url: [\(fileURL)]  \n\nfile name: [\(fileName)] \n\n file path: \(filePath)\n")
            let uploadRequest = AWSS3TransferManagerUploadRequest()
            uploadRequest.body = fileURL
            uploadRequest.key = fileName
            uploadRequest.bucket = S3BucketName
            
            self.upload(uploadRequest)
        }
    }
    
    
    // MARK: - Image picking delegates
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        print("didFinishPickingImage")
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.dismissViewControllerAnimated(true, completion: nil)
        print("didFinishPickingMediaWithInfo")
        let image = info[UIImagePickerControllerOriginalImage]
            as! UIImage
        didEditItem = true
        if tappedImage!.image!.isEqual(placeHolderImage) {
            itemImages[images.count].image = image
            images.append(image)
        }else {
            tappedImage!.image = image
            images[tappedImage!.tag] = image
        }
  
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        print("imagePickerControllerDidCancel")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - AWS S3
    
    func upload(uploadRequest: AWSS3TransferManagerUploadRequest) {
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            
                            break;
                            
                        default:
                            print("upload() failed: [\(error)]")
                            break;
                        }
                    } else {
                        print("upload() failed: [\(error)]")
                    }
                } else {
                    print("upload() failed: [\(error)]")
                }
            }
            
            if let exception = task.exception {
                print("upload() failed: [\(exception)]")
            }
            
            if task.result != nil {
                print("==============================")
                print("Task: \(task)\n\n Task Result: \(task.result)")
                print("==============================")
                print("Body: \(uploadRequest.body)")
                print("==============================")
                print("Key: \(uploadRequest.key!)")
                print("==============================")
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                    let imageURL = s3ImageBaseUrl.stringByAppendingString(uploadRequest.key!)
                    self.uploadFileURLs.append(uploadRequest.key!)
                    if self.uploadFileURLs.count == self.images.count {
                        
                        let item = self.prepareItem()
                        
                        print("==============================\n\n")
                        
                        item.printItem()
                        
                        print("==============================\n\n")
                        
                        // Send item back to ItemsViewController
                        self.delegate?.createListingViewController(self, didFinishCreatingPostForItem: item)
                    }
                })
            }
            return nil
        }
        
    }
    
    // MARK: - Private helpers
    
    /**
     Creates temporary direction for uploading images.
    */
    private func createTempUploadDir() {
        do {
            try NSFileManager.defaultManager().createDirectoryAtURL(
                NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("upload"),
                withIntermediateDirectories: true,
                attributes: nil)
        } catch let error as NSError {
            print("Creating 'upload' directory failed. Error: \(error)")
        }
    }
    
    /**
     Adds the "Done" button to the keyboard when tapped by the user.
     
     - Parameter textArea: the text input area being used by the user
     */
    private func addDoneButton(textArea: UIView) {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace,
                                            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .Done,
                                            target: view, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        
        if let textView = textArea as? UITextView {
            textView.inputAccessoryView = keyboardToolbar
        }else if let textField = textArea as? UITextField {
            textField.inputAccessoryView = keyboardToolbar
        }
        
        
    }
    
    /**
        Prepares The user with all the information added by the user
     
        - Returns: The prepared `Item`
    */
    private func prepareItem() -> Item {
        
        return Item(itemTitle: self.itemTitleTextField.text!,
                    itemPrice: Int(self.itemPriceTextField.text!)!,
                    imageURLs: self.uploadFileURLs,
                    itemDescription: self.itemDescriptionTextView.text == kPlaceHolderText ? "" : self.itemDescriptionTextView.text,
                    condition: self.itemCondition!,
                    category: self.itemCategory!,
                    acceptsOffers: self.offersSelected,
                    acceptsTrades: self.tradesSelected)
    }
    
    /**
        Displays an alert with a specified message
        
        - Parameter alertTitle: The title for the alert
        - Parameter withAlertDescription: Description of the alert
        - Parameter sender: Who called the alert
        - Returns: Void
     */
    func displayAlertMessage(alertTitle:String, withAlertDescription description:String, sender: UIViewController) -> Void {
        // hide activityIndicator view and display alert message
        let alert = UIAlertController(title: alertTitle, message: description, preferredStyle: .Alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: {(action:UIAlertAction!) in
        })
        
        alert.addAction(defaultAction)
        sender.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    /**
        Displays the activity HUD
     */
    func displayActivityIndicatorView() {
        progressHUD = ProgressHUD(text: "Creating Bazl...")
        self.view.addSubview(progressHUD!)
    }
    
    /**
        Hides the activity HUD
     */
    func stopActivityIndicatorView() {
        if let progressHUD = progressHUD {
            progressHUD.hide()
        }
    }
    
}

// MARK: - UITextField Delegate
extension CreateListingViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        activeView = nil
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        activeView = textField
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        addDoneButton(textField)
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        didEditItem = true
        
        return isTextFieldAllowToEdit(textField, withRange: range, replacementString: string)

        
    }

    /**
        Checks and returns where or not the textField is allowed another characted based on the max character count allowed.
     
        Use as helper method for `textField(:shouldChangeCharactersInRange)`
     
        - Parameter textField: `textField` passed in `textField(:shouldChangeCharactersInRange)`
        - Parameter range: `range` passed in `textField(:shouldChangeCharactersInRange)`
        - Parameter string:  `string` passed in `textField(:shouldChangeCharactersInRange)`
     
        - Returns: `true` if it has not reached the max count, `false` otherwise
    */
    func isTextFieldAllowToEdit(textField: UITextField, withRange range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        
        switch textField.tag {
            
        case 20: // Title Textfield
            
            return newLength <= itemTitleMaxCharacterCount  // Max Character count is 50
            
        case 21: // Price Textfield

            return newLength <= itemPriceMaxCharacterCount  // Max Character count is 9
            
        case 22: // Description Textfield
            return true
            
        default: // Unknown Textfield found
            return false
        }
    }
}

// MARK: - CreateListingViewController Delegate
extension CreateListingViewController: CategoriesTableViewControllerDelegate {
    
    func categoriesTableViewController(controller: CategoriesTableViewController, withCategory category: String) {
        let categoryLabel = categorySelectorView.viewWithTag(1) as! UILabel
        categoryLabel.text = category
        itemCategory = category
        didEditItem = true
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
}

// MARK: - UITextView Delegate
extension CreateListingViewController :UITextViewDelegate {
    func textViewDidEndEditing(textView: UITextView) {
        activeView = nil
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        activeView = textView
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        addDoneButton(textView)
        moveCursorToStart(textView)
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        
        if newLength > 0 {
            
            if textView.text == kPlaceHolderText {
                if text.utf16.count == 0 {
                    return false
                }
                applyNonPlaceholderStyleToTextView(textView)
                textView.text = ""
            }
            
            return true
        } else {
            applyPlaceholderStyleToTextView(textView, placeHolderText: kPlaceHolderText)
            moveCursorToStart(textView)
            return false
        }
        
        
    }
    
    /**
        Makes the text view look as a text field when created
     */
    func applyPlaceholderStyleToTextView(textView: UITextView, placeHolderText: String) {
        textView.textColor = unselectedColor
        textView.text = placeHolderText
    }
    
    /**
        Brings the text back to regular once the text view is touched
     */
    func applyNonPlaceholderStyleToTextView(textView: UITextView) {
        textView.textColor = UIColor.darkTextColor()
        textView.alpha = 1.0
    }
    
    /**
        Moves the text cursor to the beginning of the line.
     */
    func moveCursorToStart(textView: UITextView) {
        dispatch_async(dispatch_get_main_queue(), {
            textView.selectedRange = NSMakeRange(0, 0)
        })
    }
    
}





