//
//  BazlButtonMenuItem.swift
//  Bazl
//
//  Created by Hector Mejia on 5/21/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import UIKit

class BazlButtonMenuItem: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */


    var returnLocation: CGPoint! {
        didSet{
            self.center = returnLocation
        }
    }
    
    var name: String!
    var image: UIImage? {
        didSet {
            if let _ = image {
                self.setImage(image, forState: .Normal)
                self.setImage(image, forState: .Selected)
            }
        }
    }
    
}
