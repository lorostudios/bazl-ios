//
//  ItemModel.swift
//  Bazl
//
//  Created by Hector Mejia on 3/8/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import UIKit

class Item {

    
    // TODO: - FIX THIS WITH FINAL ONES
    enum ItemCondition: Int {
        case Mint = 5
        case Great = 4
        case Used = 3
        case Fair = 2
        case Worn = 1
    }
     
    private(set) var itemImageURLs: [String]!
    private(set) var itemTitle: String!
    private(set) var itemPrice: Int!
    private(set) var itemDescription: String!
    private(set) var itemCondition: ItemCondition!
    private(set) var itemCategory: String!
    private(set) var acceptsOffers: Bool!
    private(set) var acceptsTrades: Bool!
    var hasShown = false
    
    
    /**
        Creates item by specifying all the details of the item
     
        - Parameter itemTitle: Item's title
        - Parameter itemPrice: Item's price
        - Parameter imageURLs: Array of links to the uploaded images
        - Parameter itemDescription: Item's description
        - Parameter condition: Item's condition
        - Parameter category: Item's category
        - Parameter acceptsOffers: The user accepts offer
        - Parameter acceptsTrades: The user accepts trades
     */
    init(itemTitle: String, itemPrice: Int, imageURLs: [String], itemDescription: String, condition: ItemCondition, category: String, acceptsOffers: Bool, acceptsTrades: Bool) {
        self.itemPrice = itemPrice
        self.itemTitle = itemTitle
        self.itemImageURLs = imageURLs
        self.itemDescription = itemDescription
        self.itemCondition = condition
        self.itemCategory = category
        self.acceptsOffers = acceptsOffers
        self.acceptsTrades = acceptsTrades
    }
    
    /**
        Creates item from parsed JSON
     
        - Parameter dataFromAPI: Parsed JSON downloaded from server
     */
    init(dataFromAPI: NSDictionary) {
        self.itemTitle = dataFromAPI["title"] as? String
        self.itemPrice = dataFromAPI["price"] as? Int
        self.itemDescription = dataFromAPI["description"] as? String
        self.itemCondition = ItemCondition(rawValue: dataFromAPI["condition"] as! Int)
        self.itemCategory = dataFromAPI["category"] as? String
        self.itemImageURLs = dataFromAPI["images"] as? [String]
        self.acceptsOffers = dataFromAPI["acceptsOffers"] as? Bool
        self.acceptsTrades = dataFromAPI["acceptsTrades"] as? Bool
        
    }
    
    /**
        Appends a list of image URLs to an already created item
     
        - Parameter imageURLs: an array containing all the image URLs in `String` format
     */
    func appendImages(imageURLs: [String]) {
        self.itemImageURLs = imageURLs
    }
    
    /**
        Returns the first item in the image array, used as the thumbnails for each item
     
        - Returns: The first url in the image array
     */
    func getItemThumbnailImage() -> String {
        return itemImageURLs.first!
    }
    
    /**
     Returns the array containing the names of all the images for the item
     
     - Returns: The first url in the image array
     */
    func getItemImages() -> [String] {
        return itemImageURLs
    }
    
    
    /**
        Prints the contents of an item in a formatted way
     */
    func printItem() {
        print("\nPrinting information for item")
        print("\tPrice: \(self.itemPrice)")
        print("\tTitle: \(self.itemTitle)")
        print("\tCategory: \(self.itemCategory)")
        print("\tCondition: \(self.itemCondition)")
        print("\tDescription: \(self.itemDescription)")
        print("\tImages: \(itemImageURLs)")
        print("\tOffers: \(self.acceptsOffers)")
        print("\tTrades: \(self.acceptsTrades)\n")
    }
}