//
//  BazlButton.swift
//  Bazl
//
//  Created by Hector Mejia on 5/19/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import UIKit

protocol BazlButtonDelegate {
    func bazlButtonWillExpand(button: UIButton)
    func bazlButtonDidExpand(button: UIButton)
    func bazlButtonWillClose(button: UIButton)
    func bazlButtonDidClose(button: UIButton)
}

enum BazlButtonState {
    case Normal
    case Expanded
}

class BazlButton: UIButton {
        
    var bazlState: BazlButtonState = .Normal
    var menuButtons = [BazlButtonMenuItem]()
    var delegate: BazlButtonDelegate?
    
    
    func setupMenuButtons(withSelector selector: Selector, forViewController vc: UIViewController) {
        print("Setting up buttons")

        let buttonNames: [String] = [
            "profile",
            "bell",
            "bulletin",
            "messages",
            "more"
        ]
        
        
        let frame = CGRect(x: 0, y: 0, width: 30.0, height: 30.0)
        
        for name in buttonNames {

            let menuButton = BazlButtonMenuItem(type: .Custom)
            menuButton.image = UIImage(named: name)
            
            menuButton.frame = frame
            menuButton.name = name
            
            menuButton.backgroundColor = bazlBlue
            // round the corners
            menuButton.layer.cornerRadius = 15
            
            menuButton.returnLocation = self.center
            menuButton.addTarget(vc, action: selector, forControlEvents: .TouchUpInside)
            
            menuButtons.append(menuButton)
        }
        bazlState = .Normal
        
    }
    
    func expand(view: UIView) {
        
        delegate?.bazlButtonWillExpand(self)
        
        for button in menuButtons {
            view.addSubview(button)
        }
        
        let distance = self.frame.size.width
        
        UIButton.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 3.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.menuButtons[0].center.x -= distance
            
            self.menuButtons[1].center.y -= distance * 0.70
            self.menuButtons[1].center.x -= distance * 0.70
            
            self.menuButtons[2].center.y -= distance
            
            self.menuButtons[3].center.y -= distance * 0.70
            self.menuButtons[3].center.x += distance * 0.70
            
            self.menuButtons[4].center.x += distance
            
            
        }) { (finished) in
            self.bazlState = .Expanded
            self.delegate?.bazlButtonDidExpand(self)
        }
    }
    

    
    func close() {
        delegate?.bazlButtonWillClose(self)
        
        UIButton.animateWithDuration(0.2, delay: 0.0, options: .CurveEaseIn, animations: {
            for button in self.menuButtons {
                button.center = button.returnLocation
            }
            
            }) { (finished) in
                self.bazlState = .Normal
                self.delegate?.bazlButtonDidClose(self)
                for button in self.menuButtons {
                    button.removeFromSuperview()
                }
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
