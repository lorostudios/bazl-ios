//
//  Category.swift
//  Bazl
//
//  Created by Hector Mejia on 5/13/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import Foundation
import UIKit

struct Category {
    enum ItemCategory {
        case All
        case BooksAndStationary
        case CarsAndMotors
        case Electronics
        case Entertainment
        case FashionAndAccessories
        case Furniture
        case HomeAndGarden
        case Services
        case SportsAndLeisure
        
        
        func toString() -> String {
            switch self {
            case .All:
                return "All"
            case .BooksAndStationary:
                return "Books and Stationary"
            case .CarsAndMotors:
                return "Cars and Motors"
            case .Electronics:
                return "Electronics"
            case .Entertainment:
                return "Entertainment"
            case .FashionAndAccessories:
                return "Fashion and Accessories"
            case .Furniture:
                return "Furniture"
            case .HomeAndGarden:
                return "Home and Garden"
            case .Services:
                return "Services"
            case .SportsAndLeisure:
                return "Sports and Leisure"
            }
        }
        
        func itemImage() -> UIImage? {
            return UIImage(named: self.toString())
        }
    }
    
    let categoryName: ItemCategory
}