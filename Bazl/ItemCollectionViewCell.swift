//
//  ItemCollectionViewCell.swift
//  Bazl
//
//  Created by Hector Mejia on 3/7/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    var item: Item? {
        didSet {
            if let item = item {
                self.itemName.text = item.itemTitle
                self.itemPrice.text = "$\(item.itemPrice)"
                self.itemImage.image = itemImagePlaceHolders[Int(arc4random_uniform(3))]
            }
        }
    }
    
     
    // Use this method in case you want to make the cells dynamic in size.
    override func applyLayoutAttributes(layoutAttributes: UICollectionViewLayoutAttributes) {
        super.applyLayoutAttributes(layoutAttributes)
//        if let attributes = layoutAttributes as? BazlLayoutAttributes {
//            imageViewHeightLayoutConstraint.constant = attributes.photoHeight
//        }
    }
    
}
