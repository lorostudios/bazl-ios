//
//  ViewController.swift
//  Bazl
//
//  Created by Hector Mejia on 3/7/16.
//  Copyright © 2016 Loro Studios. All rights reserved.
//

import UIKit
import Alamofire
#if os(iOS)
    import AWSS3
#endif

class ItemsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    // MARK: - Instance variables
    
    var itemsList = [Item]()
    var refresherControl: UIRefreshControl!
    var cache: NSCache!
    var tappedCellIndex: Int?
    var blurEffectView: UIVisualEffectView!
    var buttonList = [UIButton]()
    
    let categoriesButton =  UIButton(type: .Custom)

    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var bazlButton: BazlButton!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!

    // MARK: - View set up functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        cache = NSCache()
        
        
        getDataFromAPI(apiURL)
        setUpBazlButton()
        setUpRefresher()
//        setup3DTouch()
        categoriesButton.frame = CGRectMake(0, 0, 100, 40) as CGRect
        categoriesButton.setTitle("All", forState: UIControlState.Normal)
        categoriesButton.addTarget(self, action: #selector(ItemsViewController.clickOnButton), forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.titleView = categoriesButton
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if bazlButton.bazlState ==  BazlButtonState.Expanded{
            handleBazlButton()
        }
    }

    // MARK: - Views
    
    func addBlurEffect() {

        // Add blur view to main view
        
        let blurEffect = UIBlurEffect(style: .Light)
        blurView.hidden = false
        blurEffectView = UIVisualEffectView(effect: blurEffect)

        blurEffectView.frame = blurView.bounds
        
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        UIView.transitionWithView(self.blurView, duration: 0.25, options: .TransitionCrossDissolve, animations: {
            self.blurView.addSubview(self.blurEffectView)
            }, completion: nil)
        

        
    }
    
    func removeBlurEffect() {
        UIView.transitionWithView(self.blurView, duration: 0.25, options: .TransitionCrossDissolve, animations: {
            self.blurEffectView.removeFromSuperview()
            }, completion: { (_) in
                self.blurView.hidden = true
            }
        )
    }
    
    // MARK: - Setup functions
    
    func setUpRefresher() {
        self.refresherControl = UIRefreshControl()
        self.refresherControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refresherControl.addTarget(self, action: #selector(ItemsViewController.refresh), forControlEvents: .ValueChanged)
        self.collectionView.addSubview(refresherControl)
    }
    
    func setUpBazlButton() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ItemsViewController.bazlButtonTapped(_:)))
        let holdGesture = UILongPressGestureRecognizer(target: self, action: #selector(ItemsViewController.bazlButtonLongPressed(_:)))
        let blurGesture = UITapGestureRecognizer(target: self, action: #selector(ItemsViewController.handleBazlButton))
        self.bazlButton.addGestureRecognizer(tapGesture)
        self.bazlButton.addGestureRecognizer(holdGesture)
        self.blurView.addGestureRecognizer(blurGesture)
        self.bazlButton.delegate = self
        
    }
    
    
    // MARK: - NavigationBar Title Button
    
    func clickOnButton() {
        print("naivgationbar title clicked")
        performSegueWithIdentifier("categoryShow", sender: self)
    }
    
    
    // MARK: - Collection View
    
    func refresh() {
        cache.removeAllObjects()
        getDataFromAPI(apiURL)
        self.refresherControl.endRefreshing()
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! ItemCollectionViewCell

        cell.item = itemsList[indexPath.row]

        if !itemsList[indexPath.row].hasShown {
            cell.contentView.alpha = 0
        }
       
        
        
        // TODO: - revisit this code block
        cell.bgView!.layer.shadowOffset = CGSizeMake(0, 0)
        cell.bgView!.layer.shadowColor = UIColor.blackColor().CGColor
        cell.bgView!.layer.shadowRadius = 4
        cell.bgView!.layer.shadowOpacity = 0.25
        cell.bgView!.layer.masksToBounds = false;
        cell.bgView!.clipsToBounds = false;
        // end of revisit block
        
        
        if (self.cache.objectForKey(indexPath.row) != nil){
            cell.itemImage.image = self.cache.objectForKey(indexPath.row) as? UIImage
        }else{
            dispatch_async(dispatch_get_main_queue(), { 
                self.downloadImage(cell.itemImage, withKey: self.itemsList[indexPath.row].getItemThumbnailImage(), forIndexPath: indexPath)
            })
        }
        
        
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        if let itemCell = cell as? ItemCollectionViewCell {
            if itemCell.item?.hasShown == false {
                UIView.animateWithDuration(1.0) {
                    itemCell.contentView.alpha = 1.0
                    itemCell.item?.hasShown = true
                }
            }
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {    
        return itemsList.count
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        tappedCellIndex = indexPath.row
        self.performSegueWithIdentifier("ShowItem", sender: self)
    }
    
    func doCollectionRefresh()
    {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let layouts = self.collectionView.collectionViewLayout as! BazlLayout
            layouts.invalidateLayout()
            layouts.clearCache()
            self.collectionView.reloadData()
            print("Refreshing table")
            return
        })
    }
    
    // MARK: - Networking
    
    func createItemListing(item: Item) {
        createItemListing(item.itemTitle, price: item.itemPrice, images: item.itemImageURLs, description: item.itemDescription, condition: item.itemCondition, category: item.itemCategory, acceptOffers: item.acceptsOffers, acceptTrades: item.acceptsTrades)
    }
    
    func createItemListing(title: String, price: Int, images: [String], description: String, condition: Item.ItemCondition, category: String, acceptOffers: Bool, acceptTrades: Bool) {
        
        let params = [
            "category": category,
            "condition": condition.rawValue,
            "description": description,
            "price": price,
            "title": title,
            "images": images,
            "acceptsOffers": acceptOffers,
            "acceptsTrades": acceptTrades
            ] as Dictionary<String, AnyObject>
        
        let headers = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        Alamofire.request(.POST, "https://lorostudios.com/api/bazl/new", parameters: params, encoding: .JSON, headers: headers)
//            .responseJSON { (response) in
//                print(response)
//            }
        .responseString { (response) in
            print(response.response)
        }

        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // TODO: - Remove after confirming Alamo POST works
    func createItemListing2(title: String, price: Int, images: [String], description: String, condition: Item.ItemCondition, category: String, acceptOffers: Bool, acceptTrades: Bool) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: "https://lorostudios.com/api/bazl/new")!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        // Required fields for POST request
        let params = [
            "category": category,
            "condition": condition.rawValue,
            "description": description,
            "price": price,
            "title": title,
            "images": images,
            "acceptsOffers": acceptOffers,
            "acceptsTrades": acceptTrades
            ] as Dictionary<String, AnyObject>
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in

            if (error != nil) {
                print("ERROR!!!\n===================\n\(error)")
            } else {
                let httpResponse = response as? NSHTTPURLResponse
                print(httpResponse)
            }
            
        })
        
        task.resume()
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func getDataFromAPI(url: String) {
        self.itemsList.removeAll()
        Alamofire.request(.GET, url)
            .responseJSON { (response) in
                
                /*************************************************************
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                *************************************************************/
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    if let jsonArray = JSON as? NSArray {
                        for item in jsonArray {
                            if let item = item as? NSDictionary {
                                self.itemsList.append(Item(dataFromAPI: item))
                            }
                        }
                        self.doCollectionRefresh()
                    }
                }
        }
    }

    
    // MARK: - AWS S3
    func downloadImage(imageView: UIImageView, withKey key: String, forIndexPath indexPath: NSIndexPath){

        var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
        
        completionHandler = { (task, location, data, error) -> Void in
            print("download finished")
            dispatch_async(dispatch_get_main_queue(), {
                if ((error) != nil){
                    print("Failed with error")
                    print("Error: \(error!)")
                }
                else{
                    //Set your image
                    if let data = data, img = UIImage(data: data) {
                        imageView.image = img
                        self.cache.setObject(img, forKey: indexPath.row)
                    }
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.defaultS3TransferUtility()
        transferUtility.downloadDataFromBucket(S3BucketName, key: key, expression: nil, completionHander: completionHandler).continueWithBlock { (task) -> AnyObject? in
            if let error = task.error {
                print("Error: \(error.localizedDescription)")
            }
            if let exception = task.exception {
                print("Exception: \(exception.description)")
            }
            if let _ = task.result {
                print("Download Starting!")
            }
            return nil;
        }
        
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "CreateItemPost" {
            let navController = segue.destinationViewController as! UINavigationController
            let destinationVC = navController.topViewController as! CreateListingViewController
            destinationVC.delegate = self
            
        } else if segue.identifier == "ShowItem" {
            let destination = segue.destinationViewController as! ItemDetailContainer
            
            if let index = tappedCellIndex {
                destination.item = itemsList[index]
                
            }
            tappedCellIndex = nil
        }
        
        else if segue.identifier == "categoryShow" {
            let navController = segue.destinationViewController as! UINavigationController
            let destinationVC = navController.topViewController as! CategoryViewController
            destinationVC.delegate = self
        }
    }

    
    // MARK: - Testing functions
    
    @IBAction func createItem(sender: UIBarButtonItem) {
        
//        createItemListing("iPhone 7", price: 200.0, description: "Really good condition, works great",
//                        condition: .Mint, category: .Books, acceptOffers: true, acceptTrades: false)
        
//        createItemLmisting(Item(itemTitle: "Hello", itemPrice: 20))
        
        
    }
    
    
    @IBAction func testbutton(sender: UIBarButtonItem) {

    }
    
    // MARK: - Private Helpers

}

// image name = "userID + date/time + filename"

// MARK: - CreateListingViewControllerDelegate functions

extension ItemsViewController: CreateListingViewControllerDelegate {
    
    func createListingViewControllerDidCancel(controller: CreateListingViewController) {
        print("Create Listing View pressed cancel")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func createListingViewController(controller: CreateListingViewController, didFinishCreatingPostForItem item: Item) {
        print("==============================")
        print("Creating item - delegate: ")
        print("==============================")
        item.printItem()
        createItemListing(item)
        
        controller.stopActivityIndicatorView()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

// MARK: - 3D Touch

extension ItemsViewController: UIViewControllerPreviewingDelegate {
    
    func setup3DTouch() {
        if( traitCollection.forceTouchCapability == .Available){
            
            registerForPreviewingWithDelegate(self, sourceView: view)
            
        }
    }
    
    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
        
    }
    
    func previewingContext(previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = collectionView?.indexPathForItemAtPoint(location) else { return nil }
        
        guard let cell = collectionView?.cellForItemAtIndexPath(indexPath) as? ItemCollectionViewCell else { return nil }
        
        guard let detailVC = storyboard?.instantiateViewControllerWithIdentifier("ItemDetailViewController") as? ItemDetailViewController else { return nil }
        
        let image = cell.itemImage.image
        detailVC.image = image
        
        detailVC.preferredContentSize = CGSize(width: 0.0, height: 300)
        
        previewingContext.sourceRect = cell.frame
        
        return detailVC
    }
    
    func previewingContext(previewingContext: UIViewControllerPreviewing, commitViewController viewControllerToCommit: UIViewController) {
        showViewController(viewControllerToCommit, sender: self)
    }
    
}

// MARK: - CategoryViewControllerDelegate functions

extension ItemsViewController: CategoryViewControllerDelegate {
    
    func categoryViewController(controller: CategoryViewController, didSelectCategoryFromCategoryList category: String, withAPIurl apiURL:String) {
        
        //changes navigationBarTitle to previously selected category
        categoriesButton.setTitle(category, forState: UIControlState.Normal)
        getDataFromAPI(apiURL)
        //refreshes UICollectionView to present items from the previously selected category
        cache.removeAllObjects()
        
        dismissCategoryViewController(controller)


    }
    
    func categoryViewControllerDidCancel(controller: CategoryViewController) {
        dismissCategoryViewController(controller)
    }
    
    func dismissCategoryViewController(controller: CategoryViewController) {
        let navView = controller.navigationController!.view
        
        UIView.animateWithDuration(0.5, animations: {
            navView.center.y = -navView.center.y
            
            let currentWindow: UIWindow = UIApplication.sharedApplication().keyWindow!
            currentWindow.addSubview(self.view)
            currentWindow.addSubview(controller.navigationController!.view)
            
        }) { (finished) in
            if finished {
                self.dismissViewControllerAnimated(false, completion: nil)
            }
        }
    }
    
}

// MARK: - Bazl Button
extension ItemsViewController: BazlButtonDelegate {
    
    func bazlButtonTapped(gesture: UITapGestureRecognizer) {
        print("bazl button tapped")
        self.performSegueWithIdentifier("CreateItemPost", sender: self)
    }
    
    func bazlButtonLongPressed(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .Began:
            handleBazlButton()
        case .Ended:
            return
        default:
            return
        }
    }
    
    func handleBazlButton() {
        
        switch bazlButton.bazlState {
        case .Normal:
            
            
            if bazlButton.menuButtons.isEmpty {
                self.bazlButton.setupMenuButtons(withSelector: #selector(ItemsViewController.handleBazlMenuButton(_:)), forViewController: self)
            }
            bazlButton.expand(self.view)
            
            
        case .Expanded:
            
            bazlButton.close()
            
        }
    }
    
    func handleBazlMenuButton(menuButton: BazlButtonMenuItem) {
        
        switch menuButton.name {
        case "profile":
            print("show profile view")
        case "bell":
            print("show notifications view")
        case "bulletin":
            print("show bullentin view")
        case "messages":
            print("show messages view")
        case "more":
            print("show options view")
        default:
            print("unknown button found")
            return
        }
        handleBazlButton()
        
    }
    
    
    func bazlButtonDidClose(button: UIButton) {
        return
    }
    
    func bazlButtonDidExpand(button: UIButton) {
        return
    }
    
    func bazlButtonWillClose(button: UIButton) {
        removeBlurEffect()
    }
    
    func bazlButtonWillExpand(button: UIButton) {
        addBlurEffect()
    }
    
}


















